# Slow Software Movement

We are part of the [slow movement in culture](https://en.wikipedia.org/wiki/Slow_movement_(culture)).

_Slow software != bad performance!_

Slow software is about a philosophy of developing and of using software. Slow software can mean:

* **Sustainable software.** Architecting and writing code in ways which are easily understandable and maintainable over time, requiring few dependencies and a rate of change that is healthy for the underlying ecosystem.
* **Thoughtful software.** Working through feature development and making decisions based on what will benefit the userbase over the long term, placing mental and social health as priority over immediate gains or selfish interests.
* **Careful software.** Seeking to understand the ways software might be used for harm, or itself be harmful by taking attention away from more important concerns in the broader culture.
* **Humanist software.** Recognizing that most software—at least in application development—is primarily written for humans to understand and reason about with ease across a wide array of skill levels, and that relying on complex code generators or "generative AI" tooling to resolve complexity instead of simply building simpler human-scale tools is an industry dead-end.
* **Open software.** Looking to established collaborative software movements like open source and the standards bodies responsible for open protocols to inspire how we build and maintain software (regardless of licensing).

## Similar or Inspiring Movements

* [Permacomputing](https://permacomputing.net/)
* [Handmade Network](https://handmade.network/)

...

## Practitioners of Slow Software

* [Jared White](https://indieweb.social/@jaredwhite)
* [Dimitri Merejkowsky](https://dmerej.info)

...

## Descriptions of Slow Software Methodologies

* [Codefol.io: Ways to Keep Software Working](https://codefol.io/posts/ways-to-keep-software-working/)
* [The Slow-Tech Manifesto](https://medium.com/qleek/the-slow-tech-manifesto-1b39fbcd1c48)
* [The Slow Code Manifesto](https://medium.com/@hannes.rollin/the-slow-code-manifesto-d0a72c35fba2)

...

## Other Resources

* [The Progress Network: Did technology kill the craftsman?](https://theprogressnetwork.org/technology-and-the-death-of-the-craftsman/)
* [Slow Computing: A Book About Taking Control of Our Digital Lives](https://www.slowcomputingbook.com)

...
